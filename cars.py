cars = ['bmw', 'audi', 'toyota', 'subaru']

# Chapter 5 modifications
for car in cars:
    if car == 'bmw':
        print(car.upper())
    else:
        print(car.title())

# Previous chapter entries

#cars.sort()
#print(cars)

#cars.sort(reverse=True)
#print(cars)

#print( "\nHere is the original list:" )
#print(cars)

#print( "\nHere is the sorted list:" )
#print(sorted(cars))

#print( "\nHere is the original list again:" )
#print(cars)

#cars.reverse()
#print(cars)
#print(len(cars))

