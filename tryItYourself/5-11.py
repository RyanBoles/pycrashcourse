num_list = [i for i in range(1,10)]
print(num_list)

for number in num_list:
    if number == 1:
        suffix = 'st'
    elif number == 2:
        suffix = 'nd'
    elif number == 3:
        suffix = 'rd'
    elif number <= 9:
        suffix = 'th'
    
    print( str(number) + suffix )
