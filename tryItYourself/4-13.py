foods = ('pizza', 'burgers', 'sushi', 'kimchi', 'balut')
for food in foods:
    print(food)

# foods[0] = 'salad'

foods = ('salad', 'chips', 'sushi', 'kimchi', 'balut')
for food in foods:
    print(food)
