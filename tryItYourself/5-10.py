current_users = ['ryan', 'mary', 'john', 'admin', 'alex']

new_users = ['Ryan', 'josh', 'sarah', 'ADMIN', 'alan']

for user in new_users:
    if user.lower() in current_users:
        print("The username '" + user + "' is already taken. Please try again.")
    else:
        print("The username '" + user + "' is available.")
