pizzas = ['cheese', 'pepperoni', 'combo']

friend_pizzas = pizzas[:]

pizzas.append('hawaiian')
friend_pizzas.append('veggie')

for pizza in pizzas:
    print("I like " + pizza + " pizza.")
for pizza in friend_pizzas:
    print("Friend likes " + pizza + " pizza.")
