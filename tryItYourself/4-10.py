longlist = []
for value in range(1,10**6):
    longlist.append(value)

print( "The first three items in the list are: " )
print( longlist[:3] )

print( "The middle three items in the list are: " )
print( longlist[499999:500002] )

print( "The last three items in the list are: " )
print( longlist[-3:] )
