# testing list conditionals
names = ['george', 'lucas', 'mary', 'ann']
user_1 = "mary"
user_2 = "ryan"
user_3 = "josh"

print("TEST 1\nIs user_1 in names? I predict true.")
print(user_1 in names)

print("\nTEST 2\nIs user_2 in names? I predict false.")
print(user_2 in names)

print("\nTEST 3\nIs user_3 in names? I predict false.")
print(user_3 in names)

print("\nTEST 4\nIs user_2 not in names? I predict true.")
print(user_2 not in names)

print("\nTEST 5\nIs user_3 not in names? I predict true.")
print(user_3 not in names)

print("\nTEST 6\nIs user_1 in names and user_2 in names and user_3 in names? I predict false.")
print(user_1 in names and user_2 in names and user_3 in names)

print("\nTEST 7\nIs user_1 in names or user_2 in names and user_3 in names? I predict true.")
print(user_1 in names or user_2 in names and user_3 in names)

print("\nTEST 8\nIs (user_1 in names or user_2 in names) and user_3 in names? I predict false.")
print((user_1 in names or user_2 in names) and user_3 in names)

print("\nTEST 9\nIs (user_1 in names or user_2 in names) and user_3 not in names? I predict true.")
print((user_1 in names or user_2 in names) and user_3 not in names)

print("\nTEST 10\nIs (user_1 in names == True) == 'True'? I predict false.")
print((user_1 in names == True) == 'True')

print("\nTEST 11\nIs user_1 in names and user_4 in name? I predict error (no shortcircuit).")
print(user_1 in names and user_4 in name)
