guests = [ 'A', 'B', 'C']

print( guests[0] + " is invited to dinner.")
print( guests[1] + " is invited to dinner.")
print( guests[2] + " is invited to dinner.")

print( "A bigger table now allows more guests.")
guests.insert(0, 'X')
guests.insert(2, 'Y')
guests.append('Z')

print( guests[0] + " is invited to dinner.")
print( guests[1] + " is invited to dinner.")
print( guests[2] + " is invited to dinner.")
print( guests[3] + " is invited to dinner.")
print( guests[4] + " is invited to dinner.")
print( guests[5] + " is invited to dinner.")
