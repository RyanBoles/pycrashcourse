guests = [ 'A', 'B', 'C']

print( guests[0] + " is invited to dinner.")
print( guests[1] + " is invited to dinner.")
print( guests[2] + " is invited to dinner.")

print( guests[1] + " will not be able to attend.")
del guests[1]
print( guests[0] + " is still invited to dinner.")
print( guests[1] + " is still invited to dinner.")
