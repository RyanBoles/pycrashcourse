age = 16

if age < 2:
    status = 'baby'
elif age < 4:
    status = 'toddler'
elif age < 13:
    status = 'kid'
elif age < 20:
    status = 'teenager'
elif age < 65:
    status = 'adult'
elif age > 65:
    status = 'elder'

print("This person is a " + status + ".")
