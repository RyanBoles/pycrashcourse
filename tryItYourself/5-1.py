car = 'subaru'
print("Is car == 'subaru'? I predict true.")
print(car == 'subaru')

print("\nIs car == 'audi'? I predict false.")
print(car == 'audi')

a = 3
b = 2*a

print("\nTEST 1\nIs a == b? I predict false.")
print(a == b)

print("\nTEST 2\nIs a > b? I predict false.")
print(a > b)

print("\nTEST 3\nIs a < b? I predict true.")
print(a < b)

print("\nTEST 4\nIs a <= b? I predict true.")
print(a <= b)

print("\nTEST 5\nIs a >= b? I predict false.")
print(a >= b)

print("\nTEST 6\nIs a != b? I predict true.")
print(a != b)

name = "ryan"
Name = "Ryan" # capitalized variables normally not ideal

print("\nTEST 7\nIs name == Name? I predict false.")
print(name == Name)

print("\nTEST 8\nIs name.title() == Name? I predict true.")
print(name.title() == Name)

print("\nTEST 9\nIs name.upper() == Name.upper()? I predict true.")
print(name.upper() == Name.upper())

print("\nTEST 10\nIs name.upper() != Name.upper()? I predict false.")
print(name.upper() != Name.upper())
