guests = [ 'A', 'B', 'C']

print( guests[0] + " is invited to dinner.")
print( guests[1] + " is invited to dinner.")
print( guests[2] + " is invited to dinner.")
print(len(guests))

print( "A bigger table now allows more guests.")
guests.insert(0, 'X')
guests.insert(2, 'Y')
guests.append('Z')

print( guests[0] + " is invited to dinner.")
print( guests[1] + " is invited to dinner.")
print( guests[2] + " is invited to dinner.")
print( guests[3] + " is invited to dinner.")
print( guests[4] + " is invited to dinner.")
print( guests[5] + " is invited to dinner.")
print(len(guests))

print( "Only two guests are now allowed.")
print( guests.pop(2) + " is no longer invited.")
print( guests.pop(2) + " is no longer invited.")
print( guests.pop(2) + " is no longer invited.")
print( guests.pop(2) + " is no longer invited.")
print(len(guests))

print( guests[0] + " is still invited to dinner.")
print( guests[1] + " is still invited to dinner.")
print(len(guests))

del guests[0]
del guests[0]
print(len(guests))

print( guests )
